import { Component } from '@angular/core';

import { environment } from '../environments/environment';
import { menu } from './routes';

@Component({
  selector: 'sensors-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'sensors';
  menu = menu;
  environment = environment;
}
