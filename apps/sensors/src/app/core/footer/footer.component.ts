import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'sensors-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() environment: string;
  constructor() { }

  ngOnInit(): void {
  }

}
