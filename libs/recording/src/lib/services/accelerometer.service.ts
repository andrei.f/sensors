import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export interface AccelerometerInfo {
  x:number;
  y:number;
  z:number;
  time: Date; // exact timestamp
  counter:number // seconds since stream started
}

@Injectable({
  providedIn: 'root'
})
export class AccelerometerService {

  getAccelerometerInfo(): Observable<AccelerometerInfo> {
    // mock
    return of({x:121, y:223, z:333, time: new Date(), counter: 1})
  }
}
