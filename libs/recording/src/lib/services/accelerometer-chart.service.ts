import { Injectable } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { AccelerometerInfo } from '../model/accelerometer.model';
import { Label } from 'ng2-charts';

@Injectable({
  providedIn: 'root'
})
export class AccelerometerChartService {

  constructor() { }

  public getChartData(coordinates: AccelerometerInfo[]): ChartDataSets[] {
    const xAxis = coordinates.map(coordinate => coordinate.x);
    const yAxis = coordinates.map(coordinate => coordinate.y);
    const zAxis = coordinates.map(coordinate => coordinate.z);
    return [{data: xAxis, label: 'X'}, {data: yAxis, label: 'Y'}, {data: zAxis, label: 'Z'}]
  }

  public getLineChartsLabels(coordinates: AccelerometerInfo[]) : Label[] {
    return coordinates.map(coordinate => coordinate.time.getMinutes() + ':' + coordinate.time.getSeconds())
  }
}
