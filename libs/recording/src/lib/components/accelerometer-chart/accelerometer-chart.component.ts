import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { AccelerometerChartService } from '../../services/accelerometer-chart.service';
import { AccelerometerInfo } from '../../model/accelerometer.model';
import { Label } from 'ng2-charts';

@Component({
  selector: 'sensors-accelerometer-chart',
  templateUrl: './accelerometer-chart.component.html',
  styles: []
})
export class AccelerometerChartComponent implements OnChanges {
  @Input() coordinates: AccelerometerInfo[];
  public chartData: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions = { responsive: true };

  constructor(private accelerometerChartService: AccelerometerChartService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.chartData = this.accelerometerChartService.getChartData(this.coordinates);
    this.lineChartLabels = this.accelerometerChartService.getLineChartsLabels(this.coordinates);
  }

}
