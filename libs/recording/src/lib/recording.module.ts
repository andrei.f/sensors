import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Dashboard } from './containers/dashboard.component';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { AccelerometerComponent } from './components/accelerometer/accelerometer.component';
import { AccelerometerChartComponent } from './components/accelerometer-chart/accelerometer-chart.component';
import { ChartsModule } from 'ng2-charts';

const routes = [
  { path: '', component: Dashboard }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), MatGridListModule, ChartsModule],
  declarations: [Dashboard, AccelerometerComponent, AccelerometerChartComponent]
})
export class RecordingModule {
}
