export interface AccelerometerInfo {
  x:number;
  y:number;
  z:number;
  time: Date; // exact timestamp
  counter:number // seconds since stream started
}
